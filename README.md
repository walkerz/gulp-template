### Node v18.18.0

### Install

```console
$ git clone https://walkerz@bitbucket.org/walkerz/gulp-template.git
$ cd gulp-template
$ npm i gulp-cli -g
$ npm run init # копирование .env.example в .env, первый билд, запуск локального сервера для разработки
```

### For MacOS

You should install fsevents

```console
npm i fsevents
```

### ENV

If you change `SOURCE_FOLDER` in `.env` file, do not forget to change source folder in `package.json` for `format` and `lint` scripts

If you change `DIST_FOLDER` in `.env` file, do not forget to add new dist folder to `.gitignore`

### Personal information

Do not forget to change personal information in `package.json`, `.env` and in `meta` tags

### Local development server

```console
$ npm run start
```

### Production build

```console
$ npm run build
```

### Run prettier

```console
$ npm run format
```

### Deploy to Gihub Pages
1. Your repo should be GitHub
2. Your links to css and other resources should be relative (./assets/css/...)

Localy on any branch run

```console
$ npm run deploy
```

Your build will appear on GitHub Pages for preview.

### Project structure

```
├── dist
│   ├── assets
│   │   ├── css
│   │   │   └── index.min.css
│   │   ├── favicons
│   │   │   ├── android-chrome-144x144.png
│   │   │   └── ...
│   │   ├── fonts
│   │   │   ├── montserrat-bold-webfont.woff
│   │   │   └── ...
│   │   └── js
│   │       └── main.bundle.js
│   └── index.html
├── gulp
│   ├── config
│   │   ├── index.js
│   │   └── paths.js
│   ├── tasks
│   │   ├── common
│   │   │   ├── browserSync.js
│   │   │   └── ...
│   │   ├── main
│   │   │   ├── build.js
│   │   │   ├── env.js
│   │   │   ├── index.js
│   │   │   └── watch.js
│   │   └── index.js
│   └── index.js
├── src
│   ├── assets
│   │   ├── favicons
│   │   │   └── favicon.png
│   │   ├── fonts
│   │   │   ├── montserrat-bold-webfont.woff
│   │   │   └── ...
│   │   ├── js
│   │   │   └── index.js
│   │   └── scss
│   │       ├── variables
│   │       │   ├── _index.scss
│   │       │   └── ...
│   │       ├── _fonts.scss
│   │       ├── _mixins.scss
│   │       └── index.scss
│   ├── html
│   │   └── pages
│   │       └── index.html
│   ├── pug
│   │   ├── components
│   │   │   ├── footer
│   │   │   │   ├── index.pug
|   |   |   |   └── ...
│   │   │   └── ...
│   │   ├── includes
│   │   │   ├── favicons.html
│   │   │   └── ...
│   │   ├── layouts
│   │   │   ├── default.pug
|   |   |   └── ...
│   │   └── pages
│   │       ├── index.pug
|   |       └── ...
│   └── twig
│       ├── components
│       │   ├── footer
│       │   │   ├── index.twig
|       |   |   └── ...
│       │   └── ...
│       ├── includes
│       │   ├── favicons.twig
│       │   └── ...
│       ├── layouts
│       │   ├── default.twig
|       |   └── ...
│       └── pages
│          ├── index.twig
|          └── ...
├── .env
├── .env.example
├── .eslintignore
├── .eslintrc
├── .gitignore
├── .prettierrc
├── gulpfile.js
├── package.json
├── package-lock.json
├── README.md
└── webpack.config.js
```
