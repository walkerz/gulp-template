import Pristine from 'pristinejs'

export const useForm = () => {
  const testForm = document.querySelector('#example-form')
  const pristine = new Pristine(testForm)

  const initPristine = () => {
    testForm.addEventListener('submit', (e) => {
      e.preventDefault()

      const isValid = pristine.validate()

      console.log(isValid)
    })
  }

  const initForm = () => {
    document.addEventListener('DOMContentLoaded', initPristine)
  }

  return {
    initForm
  }
}
